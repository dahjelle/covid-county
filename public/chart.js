function drawChart(selector, url) {
    var margin = { top: 30, right: 165, bottom: 30, left: 50 },
        width = 1200 - margin.left - margin.right,
        height = 500 - margin.top - margin.bottom,
        tooltip = { width: 100, height: 100, x: 10, y: -30 };

    var parseDate = d3.time.format("%Y-%m-%d").parse,
        bisectDate = d3.bisector(function (d) { return d.date; }).left,
        formatValue = d3.format(".2f"),
        dateFormatter = d3.time.format("%m/%d");

    var x = d3.time.scale()
        .range([0, width]);

    var y = d3.scale.linear()
        .range([height, 0]);

    var xAxis = d3.svg.axis()
        .scale(x)
        .orient("bottom")
        .tickFormat(dateFormatter);

    var yAxis = d3.svg.axis()
        .scale(y)
        .orient("left")
        .tickFormat(d3.format("s"))

    var line = d3.svg.line()
        .x(function (d) { return x(d.date); })
        .y(function (d) { return y(d.cases); });

    var svg = d3.select(selector).append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    d3.csv(url, function (error, data) {
        if (error) throw error;

        data.forEach(function (d) {
            d.date = parseDate(d.date);
            d.cases = +d.cases;
        });

        data.sort(function (a, b) {
            return a.date - b.date;
        });

        x.domain([data[0].date, data[data.length - 1].date]);
        y.domain(d3.extent(data, function (d) { return d.cases; }));

        svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis);

        //add shading based on learning model recommendations
        addGuideLines(svg, width, y);
        addShadedGuides(svg, width, y);

        // add shading for past couple of days
        addShadingToEnd(svg, height, width)

        // Add axis text - Cases per 100000
        addCasesLabel(svg, yAxis)

        // Draw the actual data line
        svg.append("path")
            .datum(data)
            .attr("class", "line")
            .attr("d", line);

        //draw circles for data points
        svg.selectAll("myCircles")
            .data(data)
            .enter()
            .append("circle")
            .attr("fill", "steelblue")
            .attr("stroke", "none")
            .attr("cx", function (d) { return x(d.date) })
            .attr("cy", function (d) { return y(d.cases) })
            .attr("r", 3)

        // Handle tooltip
        var focus = svg.append("g")
            .attr("class", "focus")
            .style("display", "none");

        focus.append("circle")
            .attr("r", 5);

        focus.append("rect")
            .attr("class", "tooltip")
            .attr("width", 150)
            .attr("height", 50)
            .attr("x", 10)
            .attr("y", -22)
            .attr("rx", 4)
            .attr("ry", 4);

        focus.append("text")
            .attr("class", "tooltip-date")
            .attr("x", 18)
            .attr("y", -2);

        focus.append("text")
            .attr("x", 18)
            .attr("y", 18)
            .text("Case Rate:");

        focus.append("text")
            .attr("class", "tooltip-cases")
            .attr("x", 90)
            .attr("y", 18);

        svg.append("rect")
            .attr("class", "overlay")
            .attr("width", width)
            .attr("height", height)
            .on("mouseover", function () { focus.style("display", null); })
            .on("mouseout", function () { focus.style("display", "none"); })
            .on("mousemove", mousemove);

        function mousemove() {
            var x0 = x.invert(d3.mouse(this)[0]),
                i = bisectDate(data, x0, 1),
                d0 = data[i - 1],
                d1 = data[i],
                d = x0 - d0.date > d1.date - x0 ? d1 : d0;
            focus.attr("transform", "translate(" + x(d.date) + "," + y(d.cases) + ")");
            focus.select(".tooltip-date").text(dateFormatter(d.date));
            focus.select(".tooltip-cases").text(formatValue(d.cases));
        }
    });
}

function addGuideLines(svg, width, y) {
    svg.append("line")
        .attr("x1", 0)
        .attr("y1", y(9))
        .attr("x2", width)
        .attr("y2", y(9))
        .attr("class", "lineYellow");
    svg.append("line")
        .attr("x1", 0)
        .attr("y1", y(19))
        .attr("x2", width)
        .attr("y2", y(19))
        .attr("class", "lineOrange");
    svg.append("line")
        .attr("x1", 0)
        .attr("y1", y(29))
        .attr("x2", width)
        .attr("y2", y(29))
        .attr("class", "lineRed");
}

function addShadedGuides(svg, width, y) {
    svg.append("rect")
        .attr("class", "shadeRed")
        .attr("width", width - 15)
        .attr("x", 0)
        .attr("y", y(30))
        .attr("height", y(29) - y(30));
    svg.append("rect")
        .attr("class", "shadeOrange")
        .attr("width", width - 15)
        .attr("x", 0)
        .attr("y", y(29))
        .attr("height", y(19) - y(29));
    svg.append("rect")
        .attr("class", "shadeYellow")
        .attr("width", width - 15)
        .attr("x", 0)
        .attr("y", y(19))
        .attr("height", y(9) - y(19));
    svg.append("rect")
        .attr("class", "shadeGreen")
        .attr("width", width - 15)
        .attr("x", 0)
        .attr("y", y(9))
        .attr("height", y(0) - y(9));
}

function addShadingToEnd(svg, height, width) {
    svg.append("rect")
        .attr("class", "lastDaysShading")
        .attr("width", 15)
        .attr("height", height)
        .attr("x", width - 15)
        .attr("y", 0)
}

function addCasesLabel(svg, yAxis) {
    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis)
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", ".71em")
        .attr("x", -20)
        .style("text-anchor", "end")
        .text("Cases Per 10000");
}