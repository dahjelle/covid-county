package covid.county.util

import java.io.File

import okhttp3.OkHttpClient
import okhttp3.Request

class CsvDownload {
    fun downloadCSVFile(sourceLink: String): File {
        val client = OkHttpClient()
        val request = Request.Builder().url(sourceLink).build()
        println("Downloading from $sourceLink...")
        val response = client.newCall(request).execute()
        val content = response.body?.string()

        val tempFile = createTempFile("covid", ".csv")
        tempFile.writeText(content ?: "Empty")

        printSize(tempFile)

        return tempFile
    }

    private fun printSize(file: File) {
        val sizeBytes = file.length()
        val sizeKB : Float = sizeBytes.toFloat() / 1000

        if(sizeKB < 1000) {
            println("Size of ${file.path} = $sizeKB KB")
        } else {
            val sizeMB : Float = sizeKB / 1000
            println("Size of ${file.path} = $sizeMB MB")
        }
    }
}
